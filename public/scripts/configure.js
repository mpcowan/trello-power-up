/* global project:false */
/* global api:false */
/* global utils:false */

var apiInput = document.querySelector('#api-input');
var tokenInput = document.querySelector('#token-input');
var saveButton = document.querySelector('.save-btn');
var autofillCheckmark = document.querySelector('#autofill-input');

function toggleButton() {
  if (apiInput.value.length > 0 && tokenInput.value.length > 0) {
    saveButton.removeAttribute('disabled');
  } else {
    saveButton.setAttribute('disabled', 'disabled');
  }
}

function saveCallback(t, command) {
  if (command === 'callback') {
    // Called from card-button, transition to next view
    project.showProjects(t);
  } else {
    // Called from settings, dismiss popup
    t.closePopup();
  }
}

function saveError(t, error) {
  throw t.NotHandled('error occurred when saving', error);
}

function saveCredentials() {
  var t = TrelloPowerUp.iframe();
  var command = t.args[0].context.command;

  utils.setAuthToken(t, apiInput.value, tokenInput.value)
    .then(saveCallback.bind(this, t, command))
    .catch(saveError.bind(this, t));
}

function validCredentialsCallback(isValid) {
  if (isValid) {
    saveCredentials();
  } else {
    if (!autofillCheckmark.checked) {
      apiInput.parentElement.classList.add('invalid');
    }
    tokenInput.parentElement.classList.add('invalid');

    saveButton.classList.remove('saving');
    saveButton.innerText = 'Save';
    saveButton.removeAttribute('disabled');
  }
}

function save() {
  saveButton.classList.add('saving');
  saveButton.innerText = 'Saving';
  saveButton.setAttribute('disabled', 'disabled');

  api.isValidCredentials(apiInput.value, tokenInput.value)
    .then(validCredentialsCallback)
    .then(utils.sizeContainer);
}

function removeInvalid() {
  if (this.parentElement.classList.contains('invalid')) {
    this.parentElement.classList.remove('invalid');
  }
}

function toggleAutofill() {
  if (this.checked) {
    window.localStorage.setItem('cached-api', apiInput.value);
    apiInput.value = 'https://gitlab.com/api/v4/';
    apiInput.setAttribute('disabled', 'disabled');
  } else {
    apiInput.value = window.localStorage.getItem('cached-api') || '';
    apiInput.removeAttribute('disabled');
  }

  removeInvalid.call(apiInput);
}

apiInput.addEventListener('input', removeInvalid);
apiInput.addEventListener('input', toggleButton);
tokenInput.addEventListener('input', removeInvalid);
tokenInput.addEventListener('input', toggleButton);
saveButton.addEventListener('click', save);
autofillCheckmark.addEventListener('change', toggleAutofill);
